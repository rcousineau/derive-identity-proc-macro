# `#[derive(Identity)]`

A first look at procedural macros in Rust.

This macro allows you to derive a default implementation of a very simple
`Identity` trait. This trait has a single `identity` method, and the provided
default version just prints the name of the struct or enum it is derived for.

```rs
#[derive(Identity)]
struct Foo;

Foo::identity();
// Identity: Foo
```

There is an example binary crate included in the **sample** directory, which
can be run with `cargo run`.
