extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn;

#[proc_macro_derive(Identity)]
pub fn identity_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse::<syn::DeriveInput>(input).expect("failed to parse macro input");
    let name = &ast.ident;
    let gen = quote! {
        impl Identity for #name {
            fn identity() {
                println!("Identity: {}", stringify!(#name));
            }
        }
    };
    TokenStream::from(gen)
}
