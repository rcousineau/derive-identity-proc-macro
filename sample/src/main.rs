use identity::Identity;
use identity_derive::Identity;

#[derive(Identity)]
struct Foo;
#[derive(Identity)]
struct Bar;
#[derive(Identity)]
enum Baz {}

fn main() {
    Foo::identity();
    Bar::identity();
    Baz::identity();
}
